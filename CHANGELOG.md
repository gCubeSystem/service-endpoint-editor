
# Changelog for VRE Manager Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.6.0-SNAPSHOT] - 2021-10-13

- Ported to git

## [v1.0.0] - 2012-05-04

- First Release